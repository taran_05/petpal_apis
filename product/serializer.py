
from product.models import *
from rest_framework import serializers
from allauth.account.adapter import get_adapter
from allauth.utils import (email_address_exists,
                               get_username_max_length)
from allauth.account import app_settings as allauth_settings
from django.utils.translation import ugettext_lazy as _
from allauth.account.utils import setup_user_email

class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('product_id','total_sales','min_price','max_price','stock_status')

class PaymentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Payment
        fields = ('address','country','state','city','zip')


class MedicineListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Medicine
        fields = ('medicine_name','synonms')

class MedicineSearchSerializer(serializers.ModelSerializer):

    class Meta:
        model = Medicine
        fields = ('medicine_name',)

class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField(
        max_length=get_username_max_length(),
        min_length=allauth_settings.USERNAME_MIN_LENGTH,
        required=allauth_settings.USERNAME_REQUIRED
    )
    user_registered = serializers.DateTimeField()
    email = serializers.EmailField(required=allauth_settings.EMAIL_REQUIRED)
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    first_name=serializers.CharField(max_length=50)

    def validate_username(self, username):
        username = get_adapter().clean_username(username)
        return username

    def validate_email(self, email):
        email = get_adapter().clean_email(email)
        if allauth_settings.UNIQUE_EMAIL:
            if email and email_address_exists(email):
                raise serializers.ValidationError(
                    _("A user is already registered with this e-mail address."))
        return email

    def validate_password1(self, password):
        return get_adapter().clean_password(password)

    def validate(self, data):
        if data['password1'] != data['password2']:
            raise serializers.ValidationError(_("The two password fields didn't match."))
        return data

    def custom_signup(self, request, user):
        pass


    def get_cleaned_data(self):
        return {
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            'first_name': self.validated_data.get('first_name', ''),
            'user_registered': self.validated_data.get('user_registered', ''),
        }
    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        self.custom_signup(request, user)
        setup_user_email(request, user, [])
        return user

class CancelOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = CancelOrder
        fields = ('order_id','user_id')


class CancelOrderDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = CancelOrder
        fields = ('order_id','user_id','is_approved','cancel_request_date','cancel_date')

class OrderItemSerailizer(serializers.ModelSerializer):

    class Meta:
        model = Order_item
        fields = ('order_item_id_1','order_item_name')


class OrderItemMetaSerializer(serializers.ModelSerializer):

    class Meta:
        model= Order_item_meta
        fields = ('meta_key','meta_value')


class PostSerializer(serializers.ModelSerializer):

    order_item_post_id = OrderItemSerailizer( many=True)
    order_item_meta_post_id = OrderItemMetaSerializer( many=True)
    class Meta:
        model = Post
        fields = ('ID','order_item_post_id','order_item_meta_post_id')

    def create(self, validated_data):
        a = validated_data.pop('order_item_post_id')
        b = validated_data.pop('order_item_meta_post_id')

        post = Post.objects.create(**validated_data)
        print (a[0]['order_item_name'])
        print(b[0])

        Order_item.objects.create(order_item_id_1 = post,order_item_name=a[0]['order_item_name'])
        # Order_item_meta.objects.create(order_item_id_2=post,meta_key=b[0]['meta_key'],meta_value=b[0]['meta_value'])
        # Order_item.objects.create(order_item_name = post)
        for order_item_meta_post_ids in b:
            Order_item_meta.objects.create(order_item_id_2 = post,**order_item_meta_post_ids)
        return post

#
# {
